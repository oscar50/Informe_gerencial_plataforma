#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  6 15:16:07 2021

@author: oscar
"""

import pandas as pd
import month_rep_FB as w_inf_FB
import month_rep_IG as w_inf_IG
import month_rep_TW as w_inf_TW


pd.options.mode.chained_assignment = None

# FOLDER_FB = "../Informes_020421/Facebook/"
# N_BRAND_FB = "facebook_lib_facebook_pages.csv"
# N_POST_FB = "facebook_lib_facebook_posts.csv"
# N_COMM_FB = "facebook_lib_facebook_comments.csv"
# ID_FACEBOOK = [384546385264076, 108430600913468, 597394330405873, 1948632145460727]
# NAME_FACEBOOK = ["Colegio Santa Inés", 'Mauricio-Guiteras-Arias', 'Johnny Torres Terzo', 'whaleandjaguar']
# BRAND_FB = ID_FACEBOOK[3]

# df = pd.read_csv('/home/oscar/Labs/Informes_020421/Facebook/facebook_lib_facebook_posts.csv')
# df.name.unique()


# DF_BRAND_FB, DF_POST_FB, DF_COM_FB = w_inf_FB.PreData(folder=FOLDER_FB,
#                                                       n_brand=N_BRAND_FB,
#                                                       n_post=N_POST_FB,
#                                                       n_comm=N_COMM_FB,
#                                                       id_brand=BRAND_FB).prepare_data()


# DF_EF_FB = w_inf_FB.EffectMetrics(df_post=DF_POST_FB,
#                                   df_brand=DF_BRAND_FB).effectiveness_metrics()

# DF_POL_FB = w_inf_FB.PolarityCalComm(df_post=DF_POST_FB,
#                                      df_com=DF_COM_FB,
#                                      text_column_name='clean_text').pol_cal()

# # DF_POL_FB = w_inf_FB.PolarityCalMonth(df_com=DF_COM_FB,
# #                                       text_column_name='clean_text').pol_cal()

# DF_EF_FB_P = w_inf_FB.GropupRenCols(df_ef=DF_EF_FB,
#                                     df_pol=DF_POL_FB,
#                                     post=True).group_rename_cols()

# DF_EF_FB_P.to_csv('/home/oscar/Labs/Informe_gerencial_plataforma/Inf_FB.csv')


# # Instagram
# FOLDER_IG = "../Informes_020421/Instagram/"
# N_BRAND_IG = "instagram_lib_profile.csv"
# N_POST_IG = "instagram_lib_posts.csv"
# N_COMM_IG = "instagram_lib_comment.csv"
# ID_INSTAGRAM = [6115274906, 35772110137, 5956919423]
# NAME_INSTAGRAM = ['colegiosantainesmlz', 'johnny.torres.terzo', 'whaleandjaguar_']
# BRAND_IG = ID_INSTAGRAM[2]

# # df = pd.read_csv('/home/oscar/Labs/Informes_020121/Instagram/instagram_lib_profile.csv')
# # df.username.unique()
# df_post = pd.read_csv('/home/oscar/Labs/Informes_020421/Instagram/instagram_lib_posts.csv')

# df = pd.read_csv(FOLDER_IG + N_POST_IG)


# DF_BRAND_IG, DF_POST_IG, DF_COM_IG = w_inf_IG.PreData(folder=FOLDER_IG,
#                                                       n_brand=N_BRAND_IG,
#                                                       n_post=N_POST_IG,
#                                                       n_comm=N_COMM_IG,
#                                                       id_brand=BRAND_IG).prepare_data()

# DF_EF_IG = w_inf_IG.EffectMetrics(df_post=DF_POST_IG,
#                                   df_brand=DF_BRAND_IG).effectiveness_metrics()

# DF_POL_IG = w_inf_IG.PolarityCalMonth(df_com=DF_COM_IG,
#                                       text_column_name='clean_text').pol_cal()

# DF_EF_IG_P = w_inf_IG.GropupRenCols(df_ef=DF_EF_IG,
#                                     df_pol=DF_POL_IG,
#                                     post=False).group_rename_cols()

# DF_EF_IG_P.to_csv('/home/oscar/Labs/Informe_gerencial_plataforma/Inf_IG.csv')


# Twitter
FOLDER_TW = "../Informes_020421/Twitter/"
N_BRAND_TW = "twitter_lib_twitteraccount.csv"
N_POST_TW = "twitter_lib_tweet.csv"
N_COMM_TW = "twitter_lib_tweetreply.csv"
ID_TWITTER = [1333185107348348936, 1268663514828333061, 905925847483285504]
NAME_TWITTER = ['Mauricio Guiteras', 'terzotorres', 'whaleandjaguar_']
BRAND_TW = ID_TWITTER[2]

df = pd.read_csv('/home/oscar/Labs/Informes_020421/Twitter/twitter_lib_tweet.csv')
df.screen_name.unique()

DF_BRAND_TW, DF_POST_TW, DF_COM_TW = w_inf_TW.PreData(folder=FOLDER_TW,
                                                      n_brand=N_BRAND_TW,
                                                      n_post=N_POST_TW,
                                                      n_comm=N_COMM_TW,
                                                      id_brand=BRAND_TW).prepare_data()

DF_EF_TW = w_inf_TW.EffectMetrics(df_post=DF_POST_TW,
                                  df_brand=DF_BRAND_TW).effectiveness_metrics()

DF_POL_TW = w_inf_TW.PolarityCalComm(df_post=DF_POST_TW,
                                      df_com=DF_COM_TW,
                                      text_column_name='clean_text').pol_cal()

# DF_POL_TW = w_inf_TW.PolarityCalMonth(df_com=DF_COM_TW,
#                                       text_column_name='clean_text').pol_cal()

DF_EF_TW_P = w_inf_TW.GropupRenCols(df_ef=DF_EF_TW,
                                    df_pol=DF_POL_TW,
                                    post=True).group_rename_cols()

DF_EF_TW_P.to_csv('/home/oscar/Labs/Informe_gerencial_plataforma/Inf_TW.csv')
