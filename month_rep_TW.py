#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 08:22:19 2021

@author: oscar
"""

import pandas as pd
from wj_analysis.common.nlp_utils import CleanText, Polarity
from wj_analysis.twitter import engagement_rates as tw_engagement_rates
from wj_analysis.common.metric_transformation import MetricCategorization
pd.options.mode.chained_assignment = None
pd.set_option('display.float_format', '{.1f}'.format)

ERR_SYS = "System error: "


def post_comments(df_post, df_com):
    '''
    post_comments extract the fields "tweet_id", "text" of the comments made by the brand

    Parameters
    ----------
    df_post : TYPE dataframe
        DESCRIPTION. with post made by brand
    df_com : TYPE dataframe
        DESCRIPTION. with comments made by brand

    Returns
    -------
    df_comments : TYPE dataframe
        DESCRIPTION. with "id_post", "message" of the comments made by the brand

    '''
    post = pd.DataFrame()
    post["tweet_id"] = df_post.tweet_id
    temp_1 = df_com[["in_reply_to_status_id", "text"]]
    temp_1 = temp_1.rename(columns={'in_reply_to_status_id': 'tweet_id'})
    df_comments = pd.merge(post, temp_1, how='inner')
    return df_comments


def clean_txt(df):
    '''
    clean_txt clean text

    Parameters
    ----------
    df : TYPE dataframe
        DESCRIPTION. with column text

    Returns
    -------
    df : TYPE dataframe
        DESCRIPTION. with column clean_text

    '''
    clean_txt = []
    for i in df.text:
        temp_1 = CleanText(input_txt=i)
        temp_2 = temp_1.process_text(rts=True,
                                     mentions=True,
                                     hashtags=True,
                                     links=True,
                                     spec_chars=True)
        clean_txt.append(temp_2)
    df["clean_text"] = clean_txt
    return df


def ext_mode(df, col_group, col_mode):
    '''
    ext_mode extracts the mode of a column and groups
    it by a column that the user selects

    Parameters
    ----------
    df : TYPE dataframe
        DESCRIPTION. with calculate mode
    col_group : TYPE string
        DESCRIPTION. column to group
    col_mode : TYPE string
        DESCRIPTION. column to calculate mode

    Returns
    -------
    df_mod : TYPE dataframe
        DESCRIPTION. with mode group by column selects

    '''
    post_id_ = df[col_group].unique()
    p_id = []
    mode = []
    for id_ in post_id_:
        temp_1 = df[df[col_group] == id_]
        temp_mod = temp_1[col_mode].mode()
        p_id.append(id_)
        mode.append(temp_mod.values[0])
    df_mod = pd.DataFrame()
    df_mod[col_group] = p_id
    df_mod[col_mode] = mode
    return df_mod


def date_day_month(df_m, date_col):
    '''
    date_day_month extract the day, month and date in short format

    Parameters
    ----------
    df_m : TYPE dataframe
        DESCRIPTION. with date column
    date_col : TYPE string
        DESCRIPTION. column with date

    Returns
    -------
    df_m : TYPE dataframe
        DESCRIPTION. with columns date, day and month
    '''

    df_m["date_n"] = df_m[date_col].apply(lambda x: x.date)
    df_m["day"] = df_m[date_col].apply(lambda x: x.day)
    df_m["month"] = df_m[date_col].apply(lambda x: x.month)

    return df_m


class PreData():

    def __init__(self, folder, n_brand, n_post, n_comm, id_brand):
        '''
        PreData prepare the data for the report calculation

        Parameters
        ----------
        folder : TYPE string
            DESCRIPTION. location of the folder to read the data
        n_brand : TYPE string
            DESCRIPTION. name file with brand data
        n_post : TYPE string
            DESCRIPTION. name file with post data
        n_comm : TYPE string
            DESCRIPTION. name file with comments data
        id_brand : TYPE int
            DESCRIPTION. id brand to analyze

        Returns
        -------
        None.

        '''

        self.folder = folder
        self.n_brand = n_brand
        self.n_post = n_post
        self.n_comm = n_comm
        self.id_brand = id_brand

    def prepare_data(self):
        '''
        PreData().prepare_data() prepare the data for the report calculation

        Returns
        -------
        df_brand_f : TYPE dataframe
            DESCRIPTION. with brand data
        df_post_f : TYPE dataframe
            DESCRIPTION. with post data
        df_com_f : TYPE dataframe
            DESCRIPTION. with comments data

        '''

        folder = self.folder
        n_brand = self.n_brand
        n_post = self.n_post
        n_comm = self.n_comm
        id_brand = self.id_brand

        df_brand = pd.read_csv(folder + n_brand, low_memory=False)
        df_brand.date = pd.to_datetime(df_brand.date)

        df_post = pd.read_csv(folder + n_post, low_memory=False)
        df_post.created_at = pd.to_datetime(df_post.created_at)

        df_com = pd.read_csv(folder + n_comm, lineterminator='\n', low_memory=False)
        df_com.created_at = pd.to_datetime(df_com.created_at)

        df_brand_f = df_brand[df_brand.twitter_id == id_brand]
        df_post_f = df_post[df_post.twitter_id == id_brand]
        df_com_f = df_com[df_com.in_reply_to_user_id == id_brand]

        df_brand_f = date_day_month(df_m=df_brand_f, date_col="date").sort_values('date_n')
        df_post_f = date_day_month(df_m=df_post_f, date_col="created_at").sort_values('date_n')
        df_com_f = date_day_month(df_m=df_com_f, date_col="created_at").sort_values('date_n')

        return df_brand_f, df_post_f, df_com_f


class PolarityCalComm():

    def __init__(self, df_post, df_com, text_column_name='clean_text'):
        '''
        PolarityCalComm calculates polarity based on comments

        Parameters
        ----------
        df_post : TYPE dataframe
            DESCRIPTION. with post
        df_com : TYPE dataframe
            DESCRIPTION. with commens of post
        text_column_name : TYPE, optional string
            DESCRIPTION. The default is 'clean_text'.

        Returns
        -------
        None.

        '''

        self.df_post = df_post
        self.df_com = df_com
        self.text_column_name = text_column_name

    def pol_cal(self):
        '''
        PolarityCalComm().pol_cal() calculates polarity based on comments

        Returns
        -------
        df_mod_pol : TYPE dataframe
            DESCRIPTION. with polarity

        '''

        pol = Polarity()
        df_post = self.df_post
        df_com = self.df_com

        df_comments = post_comments(df_post, df_com)
        df_comments = clean_txt(df_comments)
        df_comments = pol.polarity(df_text=df_comments, text_column_name='clean_text')
        df_mod_pol = ext_mode(df=df_comments, col_group="tweet_id", col_mode="polarity")

        return df_mod_pol


class PolarityCalMonth():

    def __init__(self, df_com, text_column_name='clean_text'):
        '''
        PolarityCalMonth calculates the polarity of all comments for the month

        Parameters
        ----------
        df_com : TYPE dataframe
            DESCRIPTION. with comments
        text_column_name : TYPE, optional
            DESCRIPTION. The default is 'clean_text'.

        Returns
        -------
        None.

        '''

        self.df_com = df_com
        self.text_column_name = text_column_name

    def pol_cal(self):
        '''
        PolarityCalMonth().pol_cal() calculates the polarity of all comments for the month

        Returns
        -------
        df_mod_pol : TYPE dataframe
            DESCRIPTION. with polarity

        '''

        pol = Polarity()
        df_com = self.df_com

        df_comments = clean_txt(df_com)
        df_comments = pol.polarity(df_text=df_comments, text_column_name='clean_text')
        df_mod_pol = ext_mode(df=df_comments, col_group="month", col_mode="polarity")

        return df_mod_pol


class EffectMetrics():

    def __init__(self, df_post, df_brand):
        '''
        EffectMetrics calculates relative and absolute effectiveness

        Parameters
        ----------
        df_post : TYPE dataframe
            DESCRIPTION. with post data
        df_brand : TYPE dataframe
            DESCRIPTION. with brand data

        Returns
        -------
        None.

        '''

        self.df_post = df_post
        self.df_brand = df_brand

    def effectiveness_metrics(self):
        '''
        EffectMetrics().effectiveness_metrics() calculates relative and absolute effectiveness

        Returns
        -------
        df_EF_f : TYPE dataframe
            DESCRIPTION. with relative and absolute effectiveness

        '''

        df_post = self.df_post
        df_brand = self.df_brand
        groups_tw = {}
        COLUMNS = ["screen_name", "ac_followers_count", "month", "day",
                   "engagement_rate_by_post", "tweet_id", "rel_engagement_rate_by_post"]

        if len(df_post) != 0:
            egr_bp = tw_engagement_rates.EngagementRateTW(df_post, df_brand, groups_tw, mode='terms')
            df_EF_f = egr_bp.by_post().sort_values(by=['created_at']).copy()
            metric = 'engagement_rate_by_post'
            item_col = 'twitter_id'
            df_EF_f = MetricCategorization(df_EF_f, metric, item_col, flag=0).categorize()
            df_EF_f = date_day_month(df_m=df_EF_f, date_col="created_at")
            df_EF_f = df_EF_f[COLUMNS]
        else:
            df_EF_f = pd.DataFrame(columns=[COLUMNS])
            print('no post')

        return df_EF_f


class GropupRenCols():

    def __init__(self, df_ef, df_pol, post=True):
        '''
        GropupRenCols grouped data in the data entry period

        Parameters
        ----------
        df_ef : TYPE dataframe
            DESCRIPTION. with relative and absolute effectiveness
        df_pol : TYPE dataframe
            DESCRIPTION. with polarity column
        post : TYPE, optional
            DESCRIPTION. The default is True. if the calculation method is the comments

        Returns
        -------
        None.

        '''

        self.df_ef = df_ef
        self.df_pol = df_pol
        self.post = post

    def group_rename_cols(self):
        '''
        GropupRenCols().group_rename_cols() grouped data in the data entry period,
        the months in Spanish in the dataframe

        Returns
        -------
        df_ef_pol : TYPE dataframe
            DESCRIPTION. final report

        '''

        df_ef = self.df_ef
        df_pol = self.df_pol
        post = self.post

        if len(df_ef) != 0:

            if post:

                df_ef_pol = pd.merge(df_pol, df_ef, how='right', on='tweet_id').fillna(0)
                temp_mod = ext_mode(df=df_ef_pol, col_group="month", col_mode="polarity")
                df_ef_pol = df_ef.groupby("month", as_index=False).agg({"ac_followers_count": "last",
                                                                        "screen_name": "last",
                                                                        "engagement_rate_by_post": "mean",
                                                                        "rel_engagement_rate_by_post": "mean",
                                                                        "tweet_id": "count"})

                df_ef_pol = pd.merge(df_ef_pol, temp_mod, how='right', on='month').fillna(0)

            else:
                temp_mod = df_pol
                df_ef_pol = df_ef.groupby("month", as_index=False).agg({"ac_followers_count": "last",
                                                                        "screen_name": "last",
                                                                        "engagement_rate_by_post": "mean",
                                                                        "rel_engagement_rate_by_post": "mean",
                                                                        "tweet_id": "count"})

                df_ef_pol = pd.merge(df_ef_pol, temp_mod, how='outer', on='month').fillna(0)

            df_ef_pol = df_ef_pol.rename(columns={"engagement_rate_by_post": "Efec_Ab",
                                                  "tweet_id": "Posts",
                                                  "month": "Mes",
                                                  "ac_followers_count": "Seguidores",
                                                  "screen_name": "Nombre",
                                                  'polarity': 'Polaridad',
                                                  "rel_engagement_rate_by_post": "Efec_Rel"})

            df_ef_pol.Seguidores = df_ef_pol.Seguidores.apply(lambda x: int(x))
            df_ef_pol.Efec_Rel = df_ef_pol.Efec_Rel.apply(lambda x: float(x))
            df_ef_pol.Polaridad = df_ef_pol.Polaridad.apply(lambda x: float(x))
            months = ("Enero", "Febrero", "Marzo", "Abri", "Mayo", "Junio",
                      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre")
            df_ef_pol.Mes = df_ef_pol.Mes.apply(lambda x: months[x - 1])

        else:
            df_ef_pol = pd.DataFrame(columns=["Efec_Ab", "Posts", "Mes", "Seguidores",
                                              "Nombre", 'Polaridad', "Efec_Rel"])

        return df_ef_pol
